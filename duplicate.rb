require 'fileutils'
require 'nkf'

#------------------------------------------------
# ruby duplicate.rb [xxxxxx.csv]
#------------------------------------------------

source_file, dummy = ARGV

lines = {}
File.foreach(source_file) {|line|
  line = line.split('|')

  if line.size > 1
    key = line[1].strip + '___' + NKF.nkf('-w -Z4', line[2].strip.gsub(/( |　)/, ''))

    if lines.has_key?(key)
      p key
    else
      lines[key] = line
    end
  end
}
