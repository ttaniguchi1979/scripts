hash = {}

p '1:' + hash.empty?.to_s

hash[:test1] = true
hash[:test2] = false
p hash

p '2:' + hash.empty?.to_s
p '3:' + hash[:test1].nil?.to_s
p '4:' + hash[:test3].nil?.to_s
#p '5:' + hash[:test1].blank?.to_s # rails only
