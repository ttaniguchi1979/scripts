require 'fileutils'

#------------------------------------------------
# ruby splitter.rb [xxxxxx.csv] [cnt]
#------------------------------------------------

source_file, lines_limit = ARGV
source_file_name = File.basename(source_file, '.*')
source_file_ext = File.extname(source_file)

lines = []
File.foreach(source_file) {|line|
  lines << line
}

div = lines.count.div(lines_limit.to_i)
file_count = div + 1

FileUtils.mkdir_p('./output') unless FileTest.exist?('./output')

file_no = 1
lines.each_slice(lines_limit.to_i) {|e|
  file_no_suffix = "#{sprintf('%0' + file_count.to_s.length.to_s + 'd', file_no)}"

  out_file_name = "./output/#{source_file_name}_#{file_no_suffix}#{source_file_ext}"
  File.open(out_file_name, 'w') {|f|
    f.write e.join
  }
  file_no += 1
}
